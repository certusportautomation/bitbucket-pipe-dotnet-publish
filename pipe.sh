#!/usr/bin/env bash

# Set high expectations so that Pipelines fails on an error or exit 1
set -e
set -o pipefail

printf "${HIGHLIGHT_YELLOW}Configuring GIT... \n"
git config http.${BITBUCKET_GIT_HTTP_ORIGIN}.proxy http://host.docker.internal:29418/ #because the pipe runs in docker

###################################################################
#							CONSTANTS                             #
###################################################################

#Saves root directory.
DIR_ROOT=$(pwd)

# Define colors used in building logs	 
HIGHLIGHT_YELLOW='\033[0;33m'
HIGHLIGHT_GREEN='\033[0;32m'

BITBUCKET_COMMIT_SHORT=${BITBUCKET_COMMIT::7}

DOTNET_BUILD_CONFIG=Release
DOTNET_BUILD_RID=win-x64

#Branch dependent variables.
case $BITBUCKET_BRANCH in
  master)
    VERSION_SUFFIX="master"
	NUGET_RELEASE="STABLE"
    ;;

  develop)
    VERSION_SUFFIX="develop"
	NUGET_RELEASE="PRE-RELEASE"
    ;;

  *)
    VERSION_SUFFIX="manual"
	NUGET_RELEASE="PRE-RELEASE"
    ;;
esac

###################################################################
#							BUILDING                              #
###################################################################

printf "${HIGHLIGHT_YELLOW}Setting azure artifacts credential provider... \n"
export NUGET_CREDENTIALPROVIDER_SESSIONTOKENCACHE_ENABLED=$NUGET_CREDENTIALPROVIDER_SESSIONTOKENCACHE_ENABLED
export VSS_NUGET_EXTERNAL_FEED_ENDPOINTS=$VSS_NUGET_EXTERNAL_FEED_ENDPOINTS
bash /usr/share/Utils/installcredprovider.sh

printf "${HIGHLIGHT_YELLOW}Building an testing (also includes building nuget package(s))... \n"
dotnet restore -s $DEVOPS_URL -s "https://api.nuget.org/v3/index.json" $SOLUTION_FILEPATH
dotnet build -c $DOTNET_BUILD_CONFIG $SOLUTION_FILEPATH
dotnet test --no-build -c $DOTNET_BUILD_CONFIG --filter $DOTNET_TEST_FILTER $SOLUTION_FILEPATH
dotnet clean -c $DOTNET_BUILD_CONFIG $SOLUTION_FILEPATH

###################################################################
#							VERSIONING                            #
###################################################################

#DETERMINE CURRENT VERSION
printf "${HIGHLIGHT_YELLOW}Determining current version based on previous git tags... \n"
git status
git fetch --all --tags --depth=10000
LATEST_TAG=$(git tag --list v*.*.*.*-master.* --sort=-creatordate|head -n 1)
LATEST_TAG_RELEASE=$(cut -d'.' -f3 <<<"$LATEST_TAG") # tags 3rd element when splitted by '.' (v1.2.3.4 => 3)

VERSION_RELEASE=$(($LATEST_TAG_RELEASE + 1))
VERSION_PATCH=0 #TODO PASS5DEV-1168 Support hotfixes/patches

printf "${HIGHLIGHT_YELLOW}Found tag ${LATEST_TAG}, using ${VERSION_RELEASE} as new release number \n"

#VERSION INFORMATION
VERSION="${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_RELEASE}.${VERSION_PATCH}" #5.4.3.2
INFORMATIONAL_VERSION="${VERSION}-${VERSION_SUFFIX}.${BITBUCKET_BUILD_NUMBER}" #5.4.3.2-master.342

if [ $NUGET_RELEASE = "STABLE" ] ; then
	VERSION_NUGET=$VERSION
else 
	VERSION_NUGET=$INFORMATIONAL_VERSION
fi

printf "${HIGHLIGHT_YELLOW}Tagging commit with the new version '${INFORMATIONAL_VERSION}'... \n"
git tag v${INFORMATIONAL_VERSION} ${BITBUCKET_COMMIT}
git push origin --tags

###################################################################
#							PUBLISHING					          #
###################################################################

printf "${HIGHLIGHT_YELLOW}Publishing... \n"
dotnet publish -c $DOTNET_BUILD_CONFIG --self-contained true $SOLUTION_FILEPATH -p:Version=$INFORMATIONAL_VERSION -p:AssemblyVersion=$VERSION -p:PackageVersion=$VERSION_NUGET

###################################################################
#							NUGET PACKAGES                        #
###################################################################

PACKAGES=$(find $DIR_ROOT/**/bin/Release -type f \( -name PASS.*.nupkg -o -name PASS.*.snupkg \));
if [ ${#PACKAGES[@]} -ne 0 ]; then
	for package in $PACKAGES; do 
		printf "${HIGHLIGHT_YELLOW}Pushing nuget package '$package' to Azure Devops... \n"
		dotnet nuget push $package -s $DEVOPS_URL -k $DEVOPS_APIKEY
	done
fi

###################################################################
#						INSTALLERS FOR WINDOWS                    #
###################################################################

if ls $DIR_ROOT/**/bin/Release/**/win-x64/publish >/dev/null 2>&1; then
	EXECUTABLES=$(find $DIR_ROOT/**/bin/Release/**/win-x64/publish -type f -name "PASS.*.exe" );
	if [ ${#EXECUTABLES[@]} -ne 0 ]; then
		
		#Configuring AWS CLI
		aws --version #ensuring aws installation
		aws configure set aws_access_key_id "$AWS_KEY"
		aws configure set aws_secret_access_key "$AWS_SECRET"
		
		OUTPUT_DIRECTORY="/usr/share/aws-s3-upoad";

		for EXECUTABLE in $EXECUTABLES; do 

			if [[ $EXECUTABLE == *".Tests" ]]; then
			continue
			printf "${HIGHLIGHT_YELLOW}Skipped '$EXECUTABLE'... \n"
			fi
			
			EXECUTABLE_FILENAME=$(basename -- $EXECUTABLE) #Remove file path
			MODULE_NAME=${EXECUTABLE_FILENAME%.*} #Remove file extension
			MODULE_DIR=$(dirname "${EXECUTABLE}")
			printf "${HIGHLIGHT_YELLOW}Pushing installer for '$EXECUTABLE' packaged towards AWS S3... \n"
			
			MODULE_OUTPUT=$OUTPUT_DIRECTORY/$MODULE_NAME;
			mkdir -p $MODULE_OUTPUT;
			
			printf "${HIGHLIGHT_YELLOW}Zipping published files... \n"
			cd $MODULE_DIR
			zip -r $MODULE_OUTPUT/build.zip *  # add to zip, excludes the root folder, zips subdirectories
			cd $DIR_ROOT
			
			# Copy installation files to module output directory.
			cp /usr/share/Installation/* $MODULE_OUTPUT/
			
			# Replacing placeholders in the powershell files.
			sed -i 's/PLACEHOLDER_PROJECT_NAME/'$MODULE_NAME'/g' $MODULE_OUTPUT/*.ps1

			printf "${HIGHLIGHT_YELLOW}Copying files to aws s3 bucket... \n"
			aws s3 cp $MODULE_OUTPUT s3://$AWS_S3_BUCKET_NAME/$MODULE_NAME/$BITBUCKET_BRANCH/$INFORMATIONAL_VERSION/ --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers --recursive 
		done
	fi
fi
###################################################################
#							END OF SCRIPT                         #
###################################################################

printf "${HIGHLIGHT_GREEN} Script executed succesfully! \n"
printf $? # exit code