# Bitbucket Pipelines Pipe: cepass/dotnet-publish

Publishes solution, packages executables to AWS S3, Pushes nuguet packages to Azure Devops feed.

* Builds the provided solution file. Restores packages using an external NuGet feed on Azure devops.
* Publishes the project and zips all files. 
* Uploads the zip file and powershell files for installing it as a service to the provided AWS S3 Bucket.

## Variables

| Variable              | Description           	|
| --------------------- | ----------------------------------------------------------- |
| AWS_S3_BUCKET_NAME   	| Name of the AWS S3 Bucket to place the published files in |
| AWS_KEY   		| AWS access key of the IAM user |
| AWS_SECRET   		| AWS secret key of the IAM user |
| BITBUCKET_BRANCH | Bitbucket variable for the branch name |
| BITBUCKET_BUILD_NUMBER | Bitbucket generated build number |
| BITBUCKET_COMMIT | Bitbucket variable for the git commit hash |
| BITBUCKET_GIT_HTTP_ORIGIN | Bitbucket variable for URL of the origin |
| DEVOPS_APIKEY   	| Devops api key with write access to Devops feed 	|
| DEVOPS_URL   		| URL to Azure Devops 		|
| DOTNET_TEST_FILTER   	| For excluding unit tests	|
| NUGET_CREDENTIALPROVIDER_SESSIONTOKENCACHE_ENABLED 	| Enables Session token cache |
| SOLUTION_FILEPATH     | Path to the solution file 	|
| VSS_NUGET_EXTERNAL_FEED_ENDPOINTS   			| Endpoint to the Azure DevOps environment |
| VERSION_MAJOR   			| The major version in a Major.Minor.Build.Revision format. |
| VERSION_MINOR   			| The major version in a Major.Minor.Build.Revision format. |
| TEAMS_WEBHOOK             | The teams web hook which will receive build notifications, see [Incoming Webhook configuration](https://docs.microsoft.com/en-us/microsoftteams/platform/webhooks-and-connectors/how-to/add-incoming-webhook#add-an-incoming-webhook-to-a-teams-channel) and [Cards Reference](https://docs.microsoft.com/en-us/microsoftteams/platform/task-modules-and-cards/cards/cards-reference)
## YAML Definition (Using repository variables)

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script: 
- pipe: docker://cepass/dotnet-publish
variables:
  AWS_S3_BUCKET_NAME: $AWS_S3_BUCKET_NAME
  AWS_KEY: $AWS_KEY
  AWS_SECRET: $AWS_SECRET
  BITBUCKET_BRANCH: $BITBUCKET_BRANCH
  BITBUCKET_BUILD_NUMBER: $BITBUCKET_BUILD_NUMBER
  BITBUCKET_COMMIT: $BITBUCKET_COMMIT
  BITBUCKET_GIT_HTTP_ORIGIN: $BITBUCKET_GIT_HTTP_ORIGIN
  DEVOPS_APIKEY: $DEVOPS_APIKEY
  DEVOPS_URL: $DEVOPS_URL
  DOTNET_TEST_FILTER: $DOTNET_TEST_FILTER
  NUGET_CREDENTIALPROVIDER_SESSIONTOKENCACHE_ENABLED: $NUGET_CREDENTIALPROVIDER_SESSIONTOKENCACHE_ENABLED
  SOLUTION_FILEPATH: $SOLUTION_FILEPATH
  VSS_NUGET_EXTERNAL_FEED_ENDPOINTS: $VSS_NUGET_EXTERNAL_FEED_ENDPOINTS
  VERSION_MAJOR: $VERSION_MAJOR
  VERSION_MINOR: $VERSION_MINOR
```

## Support

If you’d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by pass@certusportautomation.com.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
