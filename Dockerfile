FROM mcr.microsoft.com/dotnet/core/sdk:3.1
RUN apt-get update && apt-get install -y dos2unix
RUN apt-get install zip -y;
RUN apt-get install awscli -y;
COPY LICENSE.txt pipe.yml pipe.sh README.md /

COPY Installation /usr/share/Installation
RUN chmod +x pipe.sh
RUN dos2unix pipe.sh

COPY Utils /usr/share/Utils
RUN chmod +x /usr/share/Utils/installcredprovider.sh
RUN dos2unix /usr/share/Utils/installcredprovider.sh

ENTRYPOINT ["/pipe.sh"]